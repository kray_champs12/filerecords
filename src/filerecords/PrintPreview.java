/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filerecords;

/**
 *
 * @author anonymous
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.print.PrinterException;

import javax.swing.*;
import javax.swing.border.*;

import li.netcat.print.*;

public class PrintPreview extends JPanel implements ActionListener {
  private static final ImageIcon NEXT_PAGE = new ImageIcon("/img/NextPage.gif");
  private static final ImageIcon PREVIOUS_PAGE = new ImageIcon("/img/PreviousPage.gif");
  private static final ImageIcon ZOOM_IN = new ImageIcon("/ZoomIn.gif");
  private static final ImageIcon ZOOM_OUT = new ImageIcon("/ZoomOut.gif");
  private static final ImageIcon SCALE_OUT = new ImageIcon("ScaleOut.gif");
  private static final ImageIcon SCALE_IN = new ImageIcon("ScaleIn.gif");
  private static final ImageIcon ORIENTATION = new ImageIcon("Orientation.gif");
  private static final ImageIcon RESET = new ImageIcon("Reset.gif");
  private static final ImageIcon SETUP = new ImageIcon("Setup.gif");
  private static final ImageIcon PRINT = new ImageIcon("Print.gif");
  
  private final PreviewPanel _previewPanel;
  private final JButton      _nextPageButton;
  private final JButton      _previousPageButton;
  private final JButton      _zoomOutButton;
  private final JButton      _zoomInButton;
  private final JButton      _scaleOutButton;
  private final JButton      _scaleInButton;
  private final JButton      _orientationButton;
  private final JButton      _resetButton;
  private final JButton      _pageDialogButton;
  private final JButton      _printButton;
  //// constructors

  public PrintPreview(Print print) {
    super(new BorderLayout(5, 5));
    setBorder(new EmptyBorder(5, 5, 5, 5));
    _previewPanel = new PreviewPanel(print);
    add(new JScrollPane(_previewPanel), BorderLayout.CENTER);
    JPanel buttonPanel = new JPanel(new GridLayout(1, -1, 5, 5));
    JPanel toolBar = new JPanel(new BorderLayout(5, 5));
    toolBar.add(buttonPanel, BorderLayout.WEST);
    add(toolBar, BorderLayout.NORTH);
    buttonPanel.add(_previousPageButton = createButton(PREVIOUS_PAGE,"Previous Page"));
    buttonPanel.add(_nextPageButton = createButton(NEXT_PAGE, "Next Page"));
    buttonPanel.add(_zoomOutButton = createButton(ZOOM_OUT, "Zoom Out"));
    buttonPanel.add(_zoomInButton = createButton(ZOOM_IN, "Zoom In"));
    buttonPanel.add(_scaleOutButton = createButton(SCALE_OUT, "Scale Out"));
    buttonPanel.add(_scaleInButton = createButton(SCALE_IN, "Scale In"));
    buttonPanel.add(_orientationButton = createButton(ORIENTATION, "Change Orientation"));
    buttonPanel.add(_pageDialogButton = createButton(SETUP, "Page Setup"));
    buttonPanel.add(_resetButton = createButton(RESET, "Reset"));
    buttonPanel.add(_printButton = createButton(PRINT, "Print"));
  _nextPageButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/NextPage.gif")));
  _previousPageButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/PreviousPage.gif")));
  _zoomOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ZoomOut.gif")));
  _zoomInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ZoomIn.gif")));  
  _scaleInButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ScaleIn.gif")));
  _scaleOutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/ScaleOut.gif")));
  _orientationButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Orientation.gif")));
  _pageDialogButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Setup.gif")));
  _resetButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Reset.gif")));
  _printButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Print.gif")));
  }
  
  public void actionPerformed(ActionEvent event) {
    Object source = event.getSource();
    if (source == _nextPageButton) {
      _previewPanel.nextPage();
    }
    else if (source == _previousPageButton) {
      _previewPanel.previousPage();
    }
    else if (source == _zoomOutButton) {
      _previewPanel.setZoom(_previewPanel.getZoom() / 1.3);
    }
    else if (source == _zoomInButton) {
      _previewPanel.setZoom(_previewPanel.getZoom() * 1.3);
    }
    else if (source == _scaleOutButton) {
      _previewPanel.getPrintManager().setScaleValue(_previewPanel.getPrintManager().getScaleValue() / 1.1);
    }
    else if (source == _scaleInButton) {
      _previewPanel.getPrintManager().setScaleValue(_previewPanel.getPrintManager().getScaleValue() * 1.1);
    }
    else if (source == _orientationButton) {
      int newOrientation = _previewPanel.getPrintManager().getOrientation() == PrintConstants.PORTRAIT ? PrintConstants.LANDSCAPE : PrintConstants.PORTRAIT;
      _previewPanel.getPrintManager().setOrientation(newOrientation);
    }
    else if (source == _resetButton) {
      PrintManager pm = _previewPanel.getPrintManager();
      pm.setPageFormat(pm.getPrinterJob().defaultPage());
      pm.setScaleValue(1.0);
      _previewPanel.setZoom(1.0);
    }
    else if (source == _pageDialogButton) {
      _previewPanel.getPrintManager().pageDialog(true);
    }
    else if (source == _printButton) {
      try {
        _previewPanel.getPrintManager().print(true);
        // or call the following if you don't want a dialog:
        //_previewPanel.getPrintManager().print(false);
      }
      catch (PrinterException x) {
        x.printStackTrace();
      }
    }
  }

  //// public methods
  
  public JFrame open() {
    JFrame frame = new JFrame();
    frame.setContentPane(this);
    frame.pack();
    frame.setVisible(true);
    // the following operation causes the application to exit when
    // the user closes the print preview.Please uncoment it when you want
    // that the application should continue running 
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.toFront();
    return frame;
  }
  
  public PreviewPanel getPreviewPanel() {
    return _previewPanel;
  }
  
  //// commands
 
  private JButton createButton(ImageIcon icon, String toolTip) {
    JButton button = new JButton(icon);
    button.setToolTipText(toolTip);
    button.addActionListener(this);
    button.setPreferredSize(new Dimension(28, 26));
    return button;
  }
 
 
}
